import './Finance.css';
import TradingPair from './TradingPair';

function Finance() {
  return (
    <div className="finance">
      <h2>Cryptor courses</h2>
      <table>
          <tr>
            <th>Crypto-Asset</th>
            <th>EUR</th>
          </tr>
          <TradingPair />
          <tr>
            <td>ETH</td>
            <td>10.000</td>
          </tr>
      </table>
    </div>
  );
}

export default Finance;