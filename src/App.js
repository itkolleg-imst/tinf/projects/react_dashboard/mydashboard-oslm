import logo from './logo.svg';
import './App.css';
import Joke from './Joke/Joke.js';
import Image from './Image/Image.js';
import Finance from './Finance/Finance.js';
import TodoApp from './Todo/TodoApp.js';

function App() {
  return (
    <>
      <header>Dash.board</header>
      <main className='dashboard'>
        <Joke />
        <Image />
        <Finance />
        <TodoApp />
      </main>
    </>
  );
}

export default App;
